function getRemain() {
	return Math.ceil(((new Date("2015/10/31 0:00").getTime()) - (new Date().getTime())) / (1000 * 60 * 60 * 24));
}

function getCountdown(remain) {
	var counter = document.createElement("div");
	counter.className = "counter";
	if (remain == 0 || remain == -1) {
		var e = document.createElement("span");
		e.className = "num-" + Math.abs(remain - 1);
		counter.appendChild(e);
		var e = document.createElement("span");
		e.className = "num-d";
		counter.appendChild(e);
	} else {
		if (remain > 0) {
			var rev = ("" + remain).split("");
			if (remain < 10) {
				var x = document.createElement("span");
				x.className = "num-0";
				counter.appendChild(x);
			}
			rev.forEach(function(v) {
				var e = document.createElement("span");
				e.className = "num-" + v;
				counter.appendChild(e);
			});
		} else {
			var e = document.createElement("span");
			e.className = "num-x";
			counter.appendChild(e);
			var e = document.createElement("span");
			e.className = "num-x";
			counter.appendChild(e);
		}
	}
	return counter;
}
